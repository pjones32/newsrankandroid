package com.newsrank.parsing.tools;

public class RegistrationKeyData
{
	String registrationKey;
	String userName;
	
	boolean status;
	
	public RegistrationKeyData()
	{
		status = true;
	}
	
	public String getName()
	{
		return this.userName;
	}
	
	public void setName(String name)
	{
		this.userName = name;
	}
	
	public void setStatus(boolean status)
	{
		this.status = status;
	}
	
	public boolean getStatus()
	{
		return status;		
	}
	
	public void setApiKey(String apiKey)
	{
		this.registrationKey = apiKey;
	}
	
	public String getApiKey()
	{
		return registrationKey;
	}
}
