package com.newsrank.parsing.tools;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

public class XMLHandler extends DefaultHandler
{

	// booleans that check whether it's in a specific tag or not
	private boolean inCreated, inUse, inThrottled;
	
	private boolean inApiKey,inFailure,inName;

	// this holds the data we need
	private AccountCreationData accountCreationData;
	private RegistrationKeyData registrationKeyData;

	/**
	 * Returns the data object
	 * 
	 * @return
	 */
	public AccountCreationData getAccountCreationData()
	{
		return accountCreationData;
	}
	
	public RegistrationKeyData getRegistrationKeyData()
	{
		return registrationKeyData;
	}

	/**
	 * This gets called when the xml document is first opened
	 * 
	 * @throws SAXException
	 */
	@Override
	public void startDocument() throws SAXException
	{
		accountCreationData = new AccountCreationData();
		registrationKeyData = new RegistrationKeyData();
	}

	/**
	 * Called when it's finished handling the document
	 * 
	 * @throws SAXException
	 */
	@Override
	public void endDocument() throws SAXException
	{

	}

	/**
	 * This gets called at the start of an element. Here we're also setting the
	 * booleans to true if it's at that specific tag. (so we know where we are)
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @param atts
	 * @throws SAXException
	 */
	@Override
	public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException
	{
		
		
		
		
		
		if (localName.equals("throttled"))
		{
			inThrottled = true;
		}
		else if (localName.equals("created"))
		{
			inCreated = true;
		}
		else if (localName.equals("in_use"))
		{
			inUse = true;
		}
		
		
		else if(localName.equals("APIKey"))
		{
			inApiKey = true;
		}
		
		else if (localName.equals("failed"))
		{
			inFailure = true;
		}
		else if (localName.equals("Name"))
		{
			inName = true;
		}
		
	}

	/**
	 * Called at the end of the element. Setting the booleans to false, so we
	 * know that we've just left that tag.
	 * 
	 * @param namespaceURI
	 * @param localName
	 * @param qName
	 * @throws SAXException
	 */
	@Override
	public void endElement(String namespaceURI, String localName, String qName) throws SAXException
	{
		Log.v("endElement", localName);

		if (localName.equals("throttled"))
		{
			inThrottled = false;
		}
		else if (localName.equals("created"))
		{
			inCreated = false;
		}
		else if (localName.equals("in_use"))
		{
			inUse = false;
		}
		
		else if (localName.equals("APIKey"))
		{
			inApiKey = false;
		}
		
		else if (localName.equals("failed"))
		{
			inFailure = false;
		}
		
		else if (localName.equals("Name"))
		{
			inName = false;
		}
	}

	/**
	 * Calling when we're within an element. Here we're checking to see if there
	 * is any content in the tags that we're interested in and populating it in
	 * the Config object.
	 * 
	 * @param ch
	 * @param start
	 * @param length
	 */
	@Override
	public void characters(char ch[], int start, int length)
	{
		String chars = new String(ch, start, length);
		chars = chars.trim();

		if (inThrottled)
		{
			accountCreationData.setThrottled(Integer.parseInt(chars.toString()));
		}
		else if (inCreated)
		{
			accountCreationData.setCreated(Integer.parseInt(chars.toString()));
		}
		else if (inUse)
		{
			accountCreationData.setInUse(Integer.parseInt(chars.toString()));
		}
		
		else if (inApiKey)
		{
			registrationKeyData.setApiKey(chars.toString());
		}
		
		else if (inFailure)
		{
			registrationKeyData.setStatus(false);
		}
		
		else if (inName)
		{
			registrationKeyData.setName(chars.toString());
		}
	}
}
