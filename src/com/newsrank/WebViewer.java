package com.newsrank;

import java.io.File;
import java.util.Date;

import com.newsrank.MainActivity;
import com.newsrank.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.support.v4.app.NavUtils;

public class WebViewer extends Activity {

	private Date dateStart;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_viewer);
		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		Intent intent = getIntent();
		String websiteURL = intent.getStringExtra(MainActivity.webViewURL);
		
		WebView website = (WebView) findViewById(R.id.website);
		website.getSettings().setJavaScriptEnabled(true); 
		website.loadUrl(websiteURL);
		
		dateStart = new Date(System.currentTimeMillis());	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_web_viewer, menu);
		return true;
	}
	
	@Override
	public void finish() {
		long timeSpent = Math.abs(dateStart.getTime() - System.currentTimeMillis());
		//System.out.println("Time spent: " + timeSpent);
		Intent returnIntent = new Intent();
		returnIntent.putExtra("result", timeSpent);
		setResult(RESULT_OK,returnIntent); 
		
		super.finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			//NavUtils.navigateUpFromSameTask(this);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
