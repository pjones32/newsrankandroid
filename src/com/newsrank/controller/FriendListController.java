package com.newsrank.controller;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

import com.newsrank.MainActivity;
import com.newsrank.R;
import com.newsrank.adapters.FriendListItemAdapter;
import com.newsrank.model.FriendDo;
import com.newsrank.model.RegistrationDo;

import com.newsrank.parsing.tools.AccountCreationData;
import com.newsrank.parsing.tools.RegistrationKeyData;
import com.newsrank.singleton.SingletonNetworkController;
import com.newsrank.singleton.SingletonUserContextData;
import com.newsrank.view.FriendListView;
import com.newsrank.view.FriendListViewListener;
import com.newsrank.view.RegistrationView;
import com.newsrank.view.RegistrationViewListener;
import com.newsrank.view.UserLoginView;
import com.newsrank.view.UserLoginViewListener;

public class FriendListController extends Activity
{
	private FriendListView friendListView;
	
	// The model data
	private ArrayList<FriendDo> friendModelArrayList;
	
	// Adapter that will hold the model data
	private FriendListItemAdapter friendListAdapter;

	// server connection that will take the information
	// private abstractServerInterface serverInterface;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		
		super.onCreate(savedInstanceState);

		if (android.os.Build.VERSION.SDK_INT > 9)
		{
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}

		// Create the custom view
		friendListView = (FriendListView) View.inflate(this, R.layout.registration_view, null);
		friendListView.setFriendListViewListener(viewListener);
		
		// Create the custom adapter
		friendListAdapter = new FriendListItemAdapter(getApplication().getApplicationContext(),android.R.layout.simple_list_item_1,android.R.id.text1,friendModelArrayList);
		friendListView.setAdapterForFriendsListView(friendListAdapter);
		
		
		// friendListView.setFriendListViewAdapter();
		
		setContentView(friendListView);

	}

	@Override
	public void onStart()
	{
		super.onStart();
		
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
	}

	@Override
	public void onStop()
	{
		super.onStop();
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	private FriendListViewListener viewListener = new FriendListViewListener()
	{
		
	};
	
	

}