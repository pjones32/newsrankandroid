package com.newsrank.controller;

import com.newsrank.MainActivity;
import com.newsrank.R;
import com.newsrank.model.RegistrationDo;
import com.newsrank.parsing.tools.RegistrationKeyData;
import com.newsrank.singleton.SingletonNetworkController;
import com.newsrank.singleton.SingletonUserContextData;
import com.newsrank.view.UserLoginView;
import com.newsrank.view.UserLoginViewListener;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;

public class UserLoginController extends Activity
{
	
	private UserLoginView userLoginView;

	

	@Override
	public void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);

		if (android.os.Build.VERSION.SDK_INT > 9)
		{
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		
		// Create the custom view
		userLoginView = (UserLoginView) View.inflate(this, R.layout.user_login_view, null);
		userLoginView.setUserLoginViewListener(viewListener);
		setContentView(userLoginView);

	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		try
		{
			finalize();
		}
		catch (Throwable e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onStop()
	{
		super.onStop();
		
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
	}

	

	private UserLoginViewListener viewListener = new UserLoginViewListener()
	{
		@Override
		public RegistrationKeyData delegateToLoginController(String email, String password)
		{
			return MessageServer(email, password);
		}

		@Override
		public void  delegateToLoginTransitionControllerRegistration()
		{
			// Change to the registration activity controller
			Intent intent = new Intent(UserLoginController.this.getApplicationContext(), RegistrationController.class);
			startActivity(intent);
		}

		@Override
		public void delegateToLoginControllerTransitionToMainApplication(String email,RegistrationKeyData registrationKeyData)
		{
			
			SingletonUserContextData.getInstance().setApiKey(registrationKeyData);
			
			RegistrationDo registrationData = new RegistrationDo();
			registrationData.setEmail(email);
			String parts[] = registrationKeyData.getName().split("\\s+");
			registrationData.setFirstName(parts[0]);
			registrationData.setLastName(parts[1]);
			
			SingletonUserContextData.getInstance().setUserData(registrationData);

			Intent intent = new Intent(UserLoginController.this.getApplicationContext(), MainActivity.class);
			startActivity(intent);

			try
			{
				UserLoginController.this.finalize();
			}
			catch (Throwable e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		private RegistrationKeyData MessageServer(String email, String password)
		{
			return SingletonNetworkController.getInstance().getAccountKey(email, password);		
		}
		
	};

}
