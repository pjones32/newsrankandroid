package com.newsrank.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;

import com.newsrank.MainActivity;
import com.newsrank.R;
import com.newsrank.model.RegistrationDo;

import com.newsrank.parsing.tools.AccountCreationData;
import com.newsrank.parsing.tools.RegistrationKeyData;
import com.newsrank.singleton.SingletonNetworkController;
import com.newsrank.singleton.SingletonUserContextData;
import com.newsrank.view.RegistrationView;
import com.newsrank.view.RegistrationViewListener;
import com.newsrank.view.UserLoginView;
import com.newsrank.view.UserLoginViewListener;

public class RegistrationController extends Activity
{
	private RegistrationView registrationView;

	// server connection that will take the information
	// private abstractServerInterface serverInterface;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);

		if (android.os.Build.VERSION.SDK_INT > 9)
		{
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}

		// Create the custom view
		registrationView = (RegistrationView) View.inflate(this, R.layout.registration_view, null);
		registrationView.setUserLoginViewListener(viewListener);
		setContentView(registrationView);

	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
	}

	@Override
	public void onStop()
	{
		super.onStop();
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	private RegistrationViewListener viewListener = new RegistrationViewListener()
	{
		@Override
		public AccountCreationData delegateToRegistrationController(RegistrationDo registrationData)
		{
			return MessageServer(registrationData);
		}

		private AccountCreationData MessageServer(RegistrationDo registrationData)
		{
			return SingletonNetworkController.getInstance().createAnAccount(registrationData);
		}

		@Override
		public void delegateToRegistrationControllerTransitionToMainApplication(String email, String password)
		{
			RegistrationKeyData registrationKeyData = SingletonNetworkController.getInstance().getAccountKey(email, password);
			SingletonUserContextData.getInstance().setApiKey(registrationKeyData);

			RegistrationDo registrationData = new RegistrationDo();
			registrationData.setEmail(email);
			String parts[] = registrationKeyData.getName().split("\\s+");
			registrationData.setFirstName(parts[0]);
			registrationData.setLastName(parts[1]);

			SingletonUserContextData.getInstance().setUserData(registrationData);

			Intent intent = new Intent(RegistrationController.this.getApplicationContext(), MainActivity.class);
			startActivity(intent);

			try
			{
				RegistrationController.this.finalize();
			}
			catch (Throwable e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	};

}
