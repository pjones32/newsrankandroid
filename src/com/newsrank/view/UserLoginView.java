package com.newsrank.view;



import com.newsrank.R;
import com.newsrank.parsing.tools.RegistrationKeyData;

import android.R.string;
import android.accounts.Account;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class UserLoginView extends RelativeLayout
{
	// Buttons
	private Button loginButton;
	private Button registrationButton;
	// Edit Texts for password and user name to be sent to the model.
	private EditText passwordText;
	private EditText emailText;

	// Bridge to send information to controller
	private UserLoginViewListener viewListener;

	public UserLoginView(Context context, AttributeSet attrs)
	{
		// Call the RelativeLayout Params
		super(context, attrs);

	}

	public void setUserLoginViewListener(UserLoginViewListener viewListener)
	{
		this.viewListener = viewListener;
	}

	protected void onFinishInflate()
	{

		super.onFinishInflate();
		loginButton = (Button) findViewById(R.id.btnLogin);
		registrationButton = (Button) findViewById(R.id.btnReg);
		passwordText = (EditText) findViewById(R.id.inputPassword);
		emailText = (EditText) findViewById(R.id.inputEmail);

		loginButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (passwordText.getText().toString() == "" || emailText.getText().toString() == "")
				{
					UserLoginView.this.NotifyUser("Please Enter Your Email and Password");
				}
				else
				{
					RegistrationKeyData registrationKeyData = new RegistrationKeyData();
					registrationKeyData = viewListener.delegateToLoginController(emailText.getText().toString(), passwordText.getText().toString());

					if (registrationKeyData.getStatus() == false)
					{
						NotifyUser("Could Not Find User Account Try Password or UserName Again");
						NotifyUser("Don't have an Account? Register Today!");
					}

					else
					{
						viewListener.delegateToLoginControllerTransitionToMainApplication(emailText.getText().toString(), registrationKeyData);
					}

				}
			}

		});

		registrationButton.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// Tell Controller to make a new registration activity
				viewListener.delegateToLoginTransitionControllerRegistration();
			}
		});

	}

	public void NotifyUser(String string)
	{
		CharSequence text = string;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(this.getContext(), text, duration);
		toast.show();
	}

}
