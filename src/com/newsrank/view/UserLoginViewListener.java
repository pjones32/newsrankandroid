package com.newsrank.view;

import com.newsrank.model.RegistrationDo;
import com.newsrank.parsing.tools.RegistrationKeyData;

public interface UserLoginViewListener
{
	public RegistrationKeyData delegateToLoginController(String userName, String password);

	// Use this as a transition for now.
	public void delegateToLoginControllerTransitionToMainApplication(String email, RegistrationKeyData registrationKeyData);

	// Transition to Registration Controller
	public void delegateToLoginTransitionControllerRegistration();

}
