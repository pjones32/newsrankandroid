package com.newsrank.view;

import com.newsrank.model.RegistrationDo;
import com.newsrank.parsing.tools.AccountCreationData;

public interface RegistrationViewListener
{
	AccountCreationData delegateToRegistrationController(RegistrationDo registrationData);

	public void delegateToRegistrationControllerTransitionToMainApplication(String email, String password);
}
