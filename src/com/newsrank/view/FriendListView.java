package com.newsrank.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.newsrank.R;
import com.newsrank.model.RegistrationDo;
import com.newsrank.parsing.tools.AccountCreationData;
import com.newsrank.parsing.tools.RegistrationKeyData;


// Wrapping the friend list view to a layout that will allow more objects to exist on this layout.
public class FriendListView extends RelativeLayout
{
	
	private FriendListViewListener viewListener;
	// ListView
	private ListView friendList;

	
	// Provide this list view with an adapter to the list view.
	
	public void setAdapterForFriendsListView(ArrayAdapter adapterForFriendsList)
	{
		friendList.setAdapter(adapterForFriendsList);
	}
	
	
	public FriendListView(Context context, AttributeSet attrs)
	{
		// Call the RelativeLayout Params
		super(context, attrs);
		
		// References the actually layouts from R.newsrank.
		// Then you need layouts for the 
//		friendList = (ListView)findViewById(R.id.friendListView);
		// create what happens when the user clicks on the item....
		
		// Need button for adding friend
		
	}

	public void setFriendListViewListener(FriendListViewListener viewListener)
	{
		this.viewListener = viewListener;
	}

	protected void onFinishInflate()
	{
		
	}

	public void NotifyUser(String string)
	{
		CharSequence text = string;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(this.getContext(), text, duration);
		toast.show();
	}

}
