package com.newsrank.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.newsrank.R;
import com.newsrank.model.RegistrationDo;
import com.newsrank.parsing.tools.AccountCreationData;
import com.newsrank.parsing.tools.RegistrationKeyData;

public class RegistrationView extends RelativeLayout
{
	// Buttons
	private Button finishRegistrationButton;

	// Edit Texts for password and user name to be sent to the model.
	private EditText passwordText;
	private EditText firstNameText;
	private EditText lastNameText;
	private EditText emailText;

	// Bridge to send information to controller
	private RegistrationViewListener viewListener;

	public RegistrationView(Context context, AttributeSet attrs)
	{
		// Call the RelativeLayout Params
		super(context, attrs);

	}

	public void setUserLoginViewListener(RegistrationViewListener viewListener)
	{
		this.viewListener = viewListener;
	}

	protected void onFinishInflate()
	{
		// This is required for the new view.
		// Create any listeners here for the buttons.
		// And grab the ids from the xml file.
		// In this case the view doesn't need to be updated by the model
		// But it does need to send information to the controller.
		super.onFinishInflate();

		finishRegistrationButton = (Button) findViewById(R.id.btnReg);
		passwordText = (EditText) findViewById(R.id.inputPassword);
		firstNameText = (EditText) findViewById(R.id.inputName);
		lastNameText = (EditText) findViewById(R.id.inputLastName);
		emailText = (EditText) findViewById(R.id.inputEmail);

		finishRegistrationButton.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				RegistrationDo registrationData = new RegistrationDo();
				registrationData.setEmail(emailText.getText().toString());
				registrationData.setPassword(passwordText.getText().toString());
				registrationData.setFirstName(firstNameText.getText().toString());
				registrationData.setLastName(lastNameText.getText().toString());

				AccountCreationData accountDataResponse = new AccountCreationData();
				accountDataResponse.setAccountCreationData(viewListener.delegateToRegistrationController(registrationData));

				// Fix Later
				// if (accountDataResponse.getThrottled() == 1)
				// {
				// NotifyUser("You have Attempted To Create To Many Accounts");
				// }

				if (accountDataResponse.getCreated() == 1)
				{
					NotifyUser("Your Account was Created");
					NotifyUser("Registration Succeeded");

					viewListener.delegateToRegistrationControllerTransitionToMainApplication(registrationData.getEmail(), registrationData.getPassword());

				}

				else if (accountDataResponse.getInUse() == 1)
				{
					NotifyUser("Your Account Email is already in use");
				}

			}
		});

	}

	public void NotifyUser(String string)
	{
		CharSequence text = string;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(this.getContext(), text, duration);
		toast.show();
	}

}
