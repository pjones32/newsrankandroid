package com.newsrank;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class FragmentDesignMVP extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragment_design_mvp);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.fragment_design_mv, menu);
		return true;
	}

}
