package com.newsrank.model;

public class FriendDo
{
	private	String emailAddress;
	private	String userName;
	private String lastName;
	
	public void FriendDo(String emailAddress,String userName,String lastName)
	{
		setEmail(emailAddress);
		setFirstName(userName);
		setLastName(lastName);
	}
	
	public void setEmail(String emailAddress) 
	{
		this.emailAddress = emailAddress;
	}
	public void setFirstName(String userName)
	{
		this.userName = userName;
	}
	
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	
	public String getEmail()
	{
		return this.emailAddress;
	}
	
	public String getFirstName()
	{
		return this.userName;
	}
	
	public String getLastName()
	{
		return this.lastName;
	}
	

}
