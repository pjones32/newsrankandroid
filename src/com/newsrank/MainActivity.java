package com.newsrank;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.newsrank.singleton.SingletonUserContextData;
import com.newsrank.controller.UserLoginController;


public class MainActivity extends Activity
{
	/** Called when the activity is first created. */

	private ListView newsEntryListView;
	private ArticleAdapter articleAdapter;

	public final static String webViewURL = "";

	// ID for the articles to be passed to the server
	private String ID = "";

	private int pageNumber = 0;
	public String currentlXmlURL = "/api/v1/article/?format=xml";
	public final static String serverURL = "http://aggy.hopto.org:2300";
	private String nextXmlURL = "";
	private String prevXmlURL = "";

	private final String NewsRankPath = Environment.getExternalStorageDirectory() + "/Android/data/com.NewsRank/cache/";

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Setup the list view
		newsEntryListView = (ListView) findViewById(R.id.article_list);
		articleAdapter = new ArticleAdapter(this, R.layout.article_list_item);
		newsEntryListView.setAdapter(articleAdapter);
		// newsEntryListView.setOnScrollListener(new EndlessScrollListener());

		if (pageNumber == 0)
		{
			Button btn = (Button) findViewById(R.id.ButtonBack);
			btn.setEnabled(false);
		}
		
		// Check apiKey to see if logged in and read xml file
		String apiKey = SingletonUserContextData.getInstance().getApiKey();
		if (apiKey == null) {
		new downloadList(newsEntryListView, articleAdapter).execute(serverURL + currentlXmlURL);
		} else {
			String email = SingletonUserContextData.getInstance().getEmail();
			new downloadList(newsEntryListView, articleAdapter).execute(serverURL + "/api/v1/articlecustom/?format=xml&username="+ email + "&api_key=" + apiKey);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(0, 0, 0, "Refresh");
		menu.add(0, 1, 0, "Log In");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch (item.getItemId())
		{
		case 0:
			String apiKey = SingletonUserContextData.getInstance().getApiKey();
			if (apiKey == null) {
				new downloadList(newsEntryListView, articleAdapter).execute(serverURL + currentlXmlURL);
				} else {
					String email = SingletonUserContextData.getInstance().getEmail();
					new downloadList(newsEntryListView, articleAdapter).execute(serverURL + "/api/v1/articlecustom/?format=xml&username="+ email + "&api_key=" + apiKey);
				}
			return true;
		case 1:
			Intent intent = new Intent(getApplicationContext(), UserLoginController.class);
			startActivityForResult(intent, 1);
			return true;

		}
		return super.onOptionsItemSelected(item);

	}

	public void onDestroy()
	{
		super.onDestroy();
		if (isFinishing())
		{
			try
			{
				File imageTemp = new File(NewsRankPath + "/Images/");
				DeleteRecursive(imageTemp);

			}
			catch (Exception e)
			{
				//System.out.print("test");
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{

		if (requestCode == 1)
		{
			if (resultCode == RESULT_OK)
			{
				Long result = data.getLongExtra("result", 0);
				new sendRankingData(result, ID).execute();
				System.out.println("Time spent on article: " + ID + " is " + result);
			}
		}
	}

	public void nextButtonMethod(View view)
	{
		currentlXmlURL = serverURL + nextXmlURL;
		new downloadList(newsEntryListView, articleAdapter).execute(serverURL + nextXmlURL);

		Button btn = (Button) findViewById(R.id.ButtonBack);
		if (pageNumber == 0)
		{
			btn.setEnabled(true);
		}
		pageNumber++;
	}

	public void backButtonMethod(View view)
	{
		pageNumber--;

		currentlXmlURL = serverURL + prevXmlURL;
		new downloadList(newsEntryListView, articleAdapter).execute(serverURL + prevXmlURL);

		Button btn = (Button) findViewById(R.id.ButtonBack);
		if (pageNumber == 0)
		{
			btn.setEnabled(false);
		}
		else
		{
			btn.setEnabled(true);
		}
	}

	private class sendRankingData extends AsyncTask<String, Void, String>
	{

		Long time;
		String id;

		public sendRankingData(Long time, String id)
		{
			this.time = time;
			this.id = id;
		}
		
		@Override
		protected String doInBackground(String... params)
		{
			String res = "";

				res = sendData();
			
			return res;
		}

		@Override
		protected void onPostExecute(String result)
		{
			System.out.println("ranking Post");
		}
		
		private String sendData () {
			
			String apiKey = SingletonUserContextData.getInstance().getApiKey();
			if (apiKey != null) {
				String email = SingletonUserContextData.getInstance()
						.getEmail();
				try {
					URL url = new URL(
							"http://aggy.hopto.org:2300/api/v1/time/add/?username="
									+ email + "&api_key=" + apiKey + "&time="
									+ time + "&article=" + id + "&format=xml");

					System.out.println(url);

					// create the new connection
					HttpURLConnection urlConnection = (HttpURLConnection) url
							.openConnection();
					// set up some things on the connection
					urlConnection.setRequestMethod("GET");
					urlConnection.setDoOutput(false);
					// and connect!
					urlConnection.connect();
					InputStream inputStream = urlConnection.getInputStream();
					inputStream.close();
					urlConnection.disconnect();
					return "Data sent Successfully";
				} catch (Exception e) {
					System.out.println(e);
					return "Data not sent";
				}
			}
			return "Not logged in";
		}
	}

	private class downloadList extends AsyncTask<String, Void, String>
	{

		ListView tempListView;
		ArticleAdapter tempArticleAdapter;

		public downloadList(ListView LV, ArticleAdapter AA)
		{
			tempListView = LV;
			tempArticleAdapter = AA;
		}

		@Override
		protected String doInBackground(String... urls)
		{
			String map = null;
			for (String url : urls)
			{
				map = downloadList(url);
			}
			return map;
		}

		// Sets the Bitmap returned by doInBackground
		@Override
		protected void onPostExecute(String result)
		{

			ArrayList<Article> stories = new ArrayList<Article>(parseXML(result));

			tempArticleAdapter.clear();
			tempArticleAdapter.addAll(stories);
			tempArticleAdapter.notifyDataSetChanged();
			final ArrayList<Article> tempStories = stories;

			for (int i = 0; i < tempStories.size(); i++)
			{
				new downloadImage(articleAdapter).execute(tempStories.get(i));
			}

			tempListView.setOnItemClickListener(new OnItemClickListener()
			{
				public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				{

					String url = tempStories.get(position).getURL();
					ID = tempStories.get(position).getGUID();

					Intent intent = new Intent(getApplicationContext(), WebViewer.class);
					intent.putExtra(webViewURL, url);
					startActivityForResult(intent, 1);

				}
			});
		}

		private String downloadList(String XMLurl)
		{

			// Socket stuff
			/*
			 * Socket socket = null; DataOutputStream dataOutputStream = null;
			 * DataInputStream dataInputStream = null; BufferedReader
			 * dataInputReader; String recievedXML = "";
			 */

			try
			{
				/*
				 * socket = new Socket("aggy.hopto.org", 2400); dataOutputStream
				 * = new DataOutputStream(socket.getOutputStream());
				 * 
				 * dataInputReader = new BufferedReader(new
				 * InputStreamReader(socket.getInputStream()));
				 * //dataInputStream = new
				 * DataInputStream(socket.getInputStream());
				 * 
				 * dataOutputStream.writeUTF("SQL: SELECT * FROM Feeds LIMIT 20;"
				 * ); //recievedXML = dataInputStream.readUTF(); recievedXML =
				 * dataInputReader.readLine();
				 */

				URL url = new URL(XMLurl);
				
				System.out.println(XMLurl);
				
				// create the new connection
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				// set up some things on the connection
				urlConnection.setRequestMethod("GET");
				urlConnection.setDoOutput(false);
				// and connect!
				urlConnection.connect();
				// set the path where we want to save the file
				// in this case, going to save it on the root directory of the
				// sd card.

				File SDCardRoot = new File(NewsRankPath + "XML/");
				// create a new file, specifying the path, and the filename
				// which we want to save the file as.

				// Checks to see if the path to store images exists, if not
				// creates it
				if (!SDCardRoot.exists())
				{
					SDCardRoot.mkdirs();
				}

				File file = new File(SDCardRoot, "xmlList.xml");

				/*
				 * BufferedWriter fileOut = new BufferedWriter(new
				 * FileWriter(file)); fileOut.write(recievedXML);
				 * fileOut.close();
				 */

				// this will be used to write the downloaded data into the file
				// we created
				FileOutputStream fileOutput = new FileOutputStream(file);
				// this will be used in reading the data from the internet
				InputStream inputStream = urlConnection.getInputStream();
				// this is the total size of the file
				int totalSize = urlConnection.getContentLength();
				// variable to store total downloaded bytes
				int downloadedSize = 0;
				// create a buffer...
				byte[] buffer = new byte[1024];
				int bufferLength = 0; // used to store a temporary size of the
										// buffer
				// now, read through the input buffer and write the contents to
				// the file
				while ((bufferLength = inputStream.read(buffer)) > 0)
				{
					// add the data in the buffer to the file in the file output
					// stream (the file on the sd card
					fileOutput.write(buffer, 0, bufferLength);
					// add up the size so we know how much is downloaded
					downloadedSize += bufferLength;
					int progress = (int) (downloadedSize * 100 / totalSize);
					// this is where you would do something to report the
					// prgress, like this maybe
					// updateProgress(downloadedSize, totalSize);
				}
				String test = fileOutput.toString();

				// close the output stream when done
				fileOutput.close();
				urlConnection.disconnect();
				
			}
			catch (Exception e)
			{
				System.out.println("Error");
			}

			// Socket closing stuff
			/*
			 * finally { if (socket != null){ try { socket.close(); } catch
			 * (IOException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); } }
			 * 
			 * if (dataOutputStream != null){ try { dataOutputStream.close(); }
			 * catch (IOException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); } }
			 * 
			 * if (dataInputStream != null){ try { dataInputStream.close(); }
			 * catch (IOException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); } } }
			 */

			File xml = new File(NewsRankPath + "/xml/", "xmlList.xml");
			// Read text from file
			StringBuilder text = new StringBuilder();

			try
			{
				BufferedReader br = new BufferedReader(new FileReader(xml));
				String line;

				while ((line = br.readLine()) != null)
				{
					text.append(line);
					text.append('\n');
				}
				br.close();
			}
			catch (IOException e)
			{
				// You'll need to add proper error handling here
			}

			return text.toString();
		}

	}

	void DeleteRecursive(File fileOrDirectory)
	{
		if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles())
				DeleteRecursive(child);

		fileOrDirectory.delete();
	}

	private ArrayList<Article> parseXML(String RawXML)
	{

		ArrayList<Article> items = new ArrayList<Article>();
		try
		{
			Pattern p = Pattern.compile("<object>(.*?)</object>");
			Matcher itemMatcher = p.matcher(RawXML);

			try
			{
				Pattern pNextURL = Pattern.compile("<next.*?>(.*?)</next>");
				Matcher nextURLMatcher = pNextURL.matcher(RawXML);
				nextURLMatcher.find();
				nextXmlURL = nextURLMatcher.group(1);
				nextXmlURL = nextXmlURL.replace("&amp;", "&");
				// this might not be working correctly
			}
			catch (Exception e)
			{
				System.out.println("No next page");
			}

			try
			{
				if (pageNumber != 0)
				{
					Pattern pPrevURL = Pattern.compile("<previous.*?>(.*?)</previous>");
					Matcher prevURLMatcher = pPrevURL.matcher(RawXML);
					prevURLMatcher.find();
					prevXmlURL = prevURLMatcher.group(1);
					prevXmlURL = prevXmlURL.replace("&amp;", "&");
				}
				// this might not be working correctly
			}
			catch (Exception e)
			{
				System.out.println("No previous page");
			}

			while (itemMatcher.find())
			{
				String itemString = itemMatcher.group(1);

				// get title
				Pattern ptitle = Pattern.compile("<title.*?>(.*?)</title>");
				Matcher titleMatcher = ptitle.matcher(itemString);
				titleMatcher.find();
				String title = titleMatcher.group(1);

				// get link
				Pattern plink = Pattern.compile("<link.*?>(.*?)</link>");
				Matcher linkMatcher = plink.matcher(itemString);
				linkMatcher.find();
				String link = linkMatcher.group(1);

				// get image link
				Pattern pimageURL = Pattern.compile("<image.*?>(.*?)</image>");
				Matcher imageURLMatcher = pimageURL.matcher(itemString);
				imageURLMatcher.find();
				String imageURL = imageURLMatcher.group(1);

				// get GUID
				Pattern pGUID = Pattern.compile("<id.*?>(.*?)</id>");
				Matcher GUIDMatcher = pGUID.matcher(itemString);
				GUIDMatcher.find();
				String GUID = GUIDMatcher.group(1);

				items.add(new Article(title, link, imageURL, GUID));
			}
		}
		catch (Exception e)
		{
			System.out.println("XML parse error.");
		}
		return items;

	}

	/*
	 * public class EndlessScrollListener implements OnScrollListener {
	 * 
	 * private int visibleThreshold = 10; private int currentPage = 0; private
	 * int previousTotal = 0; private boolean loading = true;
	 * 
	 * public EndlessScrollListener() { } public EndlessScrollListener(int
	 * visibleThreshold) { this.visibleThreshold = visibleThreshold; }
	 * 
	 * @Override public void onScroll(AbsListView view, int firstVisibleItem,
	 * int visibleItemCount, int totalItemCount) { if (loading) { if
	 * (totalItemCount > previousTotal) { loading = false; previousTotal =
	 * totalItemCount; currentPage++; } } if (!loading && (totalItemCount -
	 * visibleItemCount) <= (firstVisibleItem + visibleThreshold)) { // I load
	 * the next page of gigs using a background task, // but you can call any
	 * function here. new downloadList(newsEntryListView, articleAdapter,
	 * true).execute(serverURL + nextXmlURL); loading = true; } }
	 * 
	 * @Override public void onScrollStateChanged(AbsListView view, int
	 * scrollState) { } }
	 */

	private class downloadImage extends AsyncTask<Article, Void, Void>
	{

		ArticleAdapter tempArticleAdapter;

		public downloadImage(ArticleAdapter AA)
		{
			tempArticleAdapter = AA;
		}

		@Override
		protected Void doInBackground(Article... items)
		{
			for (Article item : items)
			{
				downloadImage(item);
			}
			return null;
		}

		// Sets the Bitmap returned by doInBackground
		@Override
		protected void onPostExecute(Void v)
		{
			tempArticleAdapter.notifyDataSetChanged();
		}

		private void downloadImage(Article currentArticle)
		{

			Bitmap bm = BitmapFactory.decodeResource(null, R.drawable.no_image);
			String imagePath = Environment.getExternalStorageDirectory() + "/Android/data/com.NewsRank/cache/Images/";

			/* Open a new URL and get the InputStream to load data from it. */
			try
			{
				URL aURL = new URL(currentArticle.getImageURL());
				HttpURLConnection conn = (HttpURLConnection) aURL.openConnection();
				InputStream is = new BufferedInputStream(conn.getInputStream());
				/* Buffered is always good for a performance plus. */
				BufferedInputStream bis = new BufferedInputStream(is);
				/* Decode url-data to a bitmap. */

				bm = BitmapFactory.decodeStream(bis);

				bis.close();
				is.close();
				conn.disconnect();

				// Checks to see if the path to store images exists, if not
				// creates it
				File F = new File(imagePath);
				if (!F.exists())
				{
					F.mkdirs();
				}

				// saves image locally for use
				FileOutputStream fos = new FileOutputStream(imagePath + currentArticle.getGUID(), false);
				BufferedOutputStream bos = new BufferedOutputStream(fos);
				bm.compress(CompressFormat.JPEG, 100, bos);

				bos.flush();
				bos.close();
			}
			catch (IOException e)
			{
				Log.e("DEBUGTAG", "Remote Image Exception", e);
			}

		}

	}

}
