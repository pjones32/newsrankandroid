package com.newsrank;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Adapts NewsEntry objects onto views for lists
 */
public final class ArticleAdapter extends ArrayAdapter<Article>
{

	private final int newsItemLayoutResource;
	private final String imagePath = Environment.getExternalStorageDirectory() + "/Android/data/com.NewsRank/cache/Images/";

	public ArticleAdapter(final Context context, final int newsItemLayoutResource)
	{
		super(context, 0);
		this.newsItemLayoutResource = newsItemLayoutResource;
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent)
	{

		// We need to get the best view (re-used if possible) and then
		// retrieve its corresponding ViewHolder, which optimizes lookup
		// efficiency
		final View view = getWorkingView(convertView);
		final ViewHolder viewHolder = getViewHolder(view);
		final Article item = getItem(position);

		// Setting the title view is straightforward
		viewHolder.titleView.setText(item.getTitle());

		// Bitmap bm = null;
		File image = new File(imagePath + item.getGUID());

		// checks to see if the image is already saved locally, if not
		// downloads the image
		// if (image.exists()) {
		viewHolder.imageView.setImageBitmap(BitmapFactory.decodeFile(image.getPath()));
		/*
		 * } else { Bitmap loadingImage = BitmapFactory.decodeResource(null,
		 * R.drawable.loading);
		 * viewHolder.imageView.setImageBitmap(loadingImage); new
		 * downloadImage(viewHolder.imageView,
		 * item).execute(item.getImageURL()); }
		 */

		return view;
	}

	private View getWorkingView(final View convertView)
	{
		// The workingView is basically just the convertView re-used if possible
		// or inflated new if not possible
		View workingView = null;

		if (null == convertView)
		{
			final Context context = getContext();
			final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			workingView = inflater.inflate(newsItemLayoutResource, null);
		}
		else
		{
			workingView = convertView;
		}

		return workingView;
	}

	private ViewHolder getViewHolder(final View workingView)
	{
		// The viewHolder allows us to avoid re-looking up view references
		// Since views are recycled, these references will never change
		final Object tag = workingView.getTag();
		ViewHolder viewHolder = null;

		if (null == tag || !(tag instanceof ViewHolder))
		{
			viewHolder = new ViewHolder();

			viewHolder.imageView = (ImageView) workingView.findViewById(R.id.article_image);
			viewHolder.titleView = (TextView) workingView.findViewById(R.id.article_title);

			workingView.setTag(viewHolder);

		}
		else
		{
			viewHolder = (ViewHolder) tag;
		}

		return viewHolder;
	}

	/**
	 * ViewHolder allows us to avoid re-looking up view references Since views
	 * are recycled, these references will never change
	 */
	private static class ViewHolder
	{
		public TextView titleView;
		public ImageView imageView;
	}

	private class downloadImage extends AsyncTask<String, Void, Bitmap>
	{

		ImageView tempIV;
		Article tempItem;

		public downloadImage(ImageView IV, Article item)
		{
			tempIV = IV;
			tempItem = item;
		}

		@Override
		protected Bitmap doInBackground(String... urls)
		{
			Bitmap map = null;
			for (String url : urls)
			{
				map = downloadImage(url);
			}
			return map;
		}

		// Sets the Bitmap returned by doInBackground
		@Override
		protected void onPostExecute(Bitmap result)
		{
			tempIV.setImageBitmap(result);
		}

		private Bitmap downloadImage(String imageURL)
		{

			Bitmap bm = BitmapFactory.decodeResource(null, R.drawable.no_image);

			/* Open a new URL and get the InputStream to load data from it. */
			try
			{
				URL aURL = new URL(imageURL);
				HttpURLConnection conn = (HttpURLConnection) aURL.openConnection();
				InputStream is = new BufferedInputStream(conn.getInputStream());
				/* Buffered is always good for a performance plus. */
				BufferedInputStream bis = new BufferedInputStream(is);
				/* Decode url-data to a bitmap. */

				bm = BitmapFactory.decodeStream(bis);

				bis.close();
				is.close();
				conn.disconnect();

				// Checks to see if the path to store images exists, if not
				// creates it
				File F = new File(imagePath);
				if (!F.exists())
				{
					F.mkdirs();
				}

				// saves image locally for use
				FileOutputStream fos = new FileOutputStream(imagePath + tempItem.getGUID(), false);
				BufferedOutputStream bos = new BufferedOutputStream(fos);
				bm.compress(CompressFormat.JPEG, 100, bos);

				bos.flush();
				bos.close();
			}
			catch (IOException e)
			{
				Log.e("DEBUGTAG", "Remote Image Exception", e);
				return bm;
			}
			return bm;

		}

	}

}
