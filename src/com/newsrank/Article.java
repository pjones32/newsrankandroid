package com.newsrank;

/**
 * Encapsulates information about a news entry
 */
public final class Article {
	
	private final String title;
	private final String imageURL;
	private final String URL;
	private final String GUID;

	public Article(final String title, final String URL, final String imageURL, final String GUID) {
		this.title = title;
		this.URL = URL;
		this.imageURL = imageURL;
		this.GUID = GUID;
	}

	/**
	 * @return Title of news entry
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * @return image of news entry
	 */
	public String getImageURL() {
		return imageURL;
	}

	/**
	 * @return URL of news entry
	 */
	public String getURL() {
		return URL;
	}
	
	/**
	 * @return GUID of news entry
	 */
	public String getGUID() {
		return GUID;
	}

}