package com.newsrank.singleton;

import com.newsrank.model.RegistrationDo;
import com.newsrank.parsing.tools.RegistrationKeyData;

public class SingletonUserContextData
{
	private String apiKey;
	
	private String firstName;
	private String lastName;
	private String email;
	
	static SingletonUserContextData instance = new SingletonUserContextData();
	
	public static SingletonUserContextData getInstance()
	{
		return instance;
	}
	
	public void setApiKey(RegistrationKeyData objectContainingApiKey)
	{
		this.apiKey = objectContainingApiKey.getApiKey();
	}
	
	public void setUserData(RegistrationDo objectContainingUserData)
	{
		this.firstName = objectContainingUserData.getFirstName();
		this.lastName = objectContainingUserData.getLastName();
		this.email = objectContainingUserData.getEmail();
	}

	
	public String getApiKey() {
		return apiKey;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	
}
