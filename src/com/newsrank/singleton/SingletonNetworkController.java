package com.newsrank.singleton;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.os.AsyncTask;

import com.newsrank.model.RegistrationDo;
import com.newsrank.parsing.tools.AccountCreationData;
import com.newsrank.parsing.tools.RegistrationKeyData;
import com.newsrank.parsing.tools.XMLHandler;

public class SingletonNetworkController
{
	

	private static SingletonNetworkController instance = new SingletonNetworkController();

	public static SingletonNetworkController getInstance()
	{
		return instance;
	}

	public RegistrationKeyData getAccountKey(String email,String password)
	{
		RegistrationKeyData data;
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet("http://aggy.hopto.org:2300/api/v1/user/reqkey/?format=xml&email="+email+"&password="+password);
		try
		{
			// Read the input from the response xml
			HttpResponse response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();

			SAXParserFactory saxPF = SAXParserFactory.newInstance();

			SAXParser saxP = null;
			try
			{
				saxP = saxPF.newSAXParser();
			}
			catch (ParserConfigurationException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (SAXException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			XMLReader xmlR = null;
			try
			{
				xmlR = saxP.getXMLReader();
			}
			catch (SAXException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Use my custom xml handler for all xml handling
			XMLHandler myXMLHandler = new XMLHandler();

			xmlR.setContentHandler(myXMLHandler);
			try
			{
				xmlR.parse(new InputSource(entity.getContent()));
			}
			catch (IllegalStateException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (SAXException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			data = myXMLHandler.getRegistrationKeyData();

			// This means it failed to register because the name is already in
			// use.
			
			// else return true and tell about registration success
			
			return data;
			

		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Fail anyways since the try didn't at all work
		return null;

	}

	public AccountCreationData createAnAccount(RegistrationDo registrationData)
	{
		// Send in the registration data and hope for a response 
		AccountCreationData data;
		DefaultHttpClient httpclient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(
				"http://aggy.hopto.org:2300/api/v1/user/signup/?format=xml" +
				"&email="+registrationData.getEmail()+
				"&password="+registrationData.getPassword()+
				"&first="+registrationData.getFirstName()+
				"&last="+registrationData.getLastName());
		try
		{
			// Read the input from the response xml
			HttpResponse response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();

			SAXParserFactory saxPF = SAXParserFactory.newInstance();

			SAXParser saxP = null;
			try
			{
				saxP = saxPF.newSAXParser();
			}
			catch (ParserConfigurationException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (SAXException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			XMLReader xmlR = null;
			try
			{
				xmlR = saxP.getXMLReader();
			}
			catch (SAXException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Use my custom xml handler for all xml handling
			XMLHandler myXMLHandler = new XMLHandler();

			xmlR.setContentHandler(myXMLHandler);
			try
			{
				xmlR.parse(new InputSource(entity.getContent()));
			}
			catch (IllegalStateException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (SAXException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			data = myXMLHandler.getAccountCreationData();

			return data;

		}
		catch (ClientProtocolException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Fail anyways since the try didn't at all work
		return null;

	}
	

	

}
