import os
import sys

path = '/home/aggy/development/aggy'
if path not in sys.path:
  sys.path.append(path)
  sys.path.append('/home/aggy/development/aggy/aggy')
  sys.path.append('/home/aggy/development/aggy/news')


os.environ['DJANGO_SETTINGS_MODULE'] = 'aggy.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

