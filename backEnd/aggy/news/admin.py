from news.models import Article
from news.models import Friends
from news.models import Liked
from news.models import CustomArticle
from django.contrib import admin

admin.site.register(Article)
admin.site.register(Friends)
admin.site.register(Liked)
admin.site.register(CustomArticle)
