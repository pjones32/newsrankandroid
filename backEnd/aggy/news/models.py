from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Article(models.Model):
  title = models.TextField()
  link = models.URLField()
  description = models.TextField(blank=True)
  image = models.URLField(blank=True)
  pubdate = models.FloatField(db_index=True)
  rank = models.IntegerField(db_index=True)
  category = models.CharField(max_length=30)

  class Meta:
    ordering = ['-rank', '-pubdate']

  def __unicode__(self):
    return self.title

class Friends(models.Model):
  user = models.ForeignKey(User, db_index=True)
  friend = models.ForeignKey(User, related_name='friend')
  status = models.BooleanField()

  def __unicode__(self):
    return self.user.username + " -> " + self.friend.username

class Liked(models.Model):
  user = models.ForeignKey(User, db_index=True)
  article = models.ForeignKey(Article, db_index=True)
  time = models.IntegerField()

class CustomArticle(models.Model):
  user = models.ForeignKey(User, db_index=True)
  article = models.ForeignKey(Article, db_index=True)
  rank = models.IntegerField(db_index=True)

  class Meta:
    ordering = ['-rank']

class Category(models.Model):
  user = models.ForeignKey(User, db_index=True)
  category = models.CharField(max_length=30,db_index=True)


