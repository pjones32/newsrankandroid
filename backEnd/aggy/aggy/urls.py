from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from django.conf.urls.defaults import *
from tastypie.api import Api
from aggy.api import ArticleResource
from aggy.api import CustomArticleResource
from aggy.api import UserResource
from aggy.api import FriendListResource
from aggy.api import FriendResource
from aggy.api import ATimeResource

v1_api = Api(api_name='v1')
v1_api.register(ArticleResource())
v1_api.register(CustomArticleResource())
v1_api.register(UserResource())
v1_api.register(FriendListResource())
v1_api.register(FriendResource())
v1_api.register(ATimeResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'aggy.views.home', name='home'),
    # url(r'^aggy/', include('aggy.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(v1_api.urls)),
)
