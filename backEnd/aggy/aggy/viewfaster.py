from django.contrib.auth.models import User
from news.models import CustomArticle
from news.models import Article
from news.models import Friends
from news.models import Liked

class CustomView:

  def addArticle(self, article, time, user):
    try:
      theArticle = CustomArticle.objects.get(user=user, article=article);
      theArticle.rank = theArticle.rank + article.rank
      theArticle.save()
    except CustomArticle.DoesNotExist:
      theArticle = CustomArticle(user=user, article=article, rank=time)
      theArticle.save()

  def runCode(self, username):
    myFriends = self.friends.filter(user=self.user)

    for i in myFriends:
      myLiked = self.liked.filter(user=i.friend)
      for j in myLiked:
        self.addArticle(j.article, j.time, self.user)

    for i in self.articles:
      self.addArticle(i, i.rank, self.user)

  def __init__(self, username):
    self.user = User.objects.get(username__exact=username)
    CustomArticle.objects.filter(user=self.user).delete()
    self.articles = Article.objects.all()
    self.friends = Friends.objects.all()
    self.liked = Liked.objects.all()
    self.runCode(username)

