#!/usr/bin/python

#UPDATED 11/03/2013
#CHANGED - sqlite3 to mySQL and all functions relating

#UPDATED 27/02/2013
#ADDED - function to remove duplicates.

#UPDATED 26/02/2013
#FIXED BUG - date purge wasn't working.
#FIXED BUG - removed cdata in title.
#CHANGED - pubdate now stored as epoch time.
#CHANGED - _RSSXMLTimeCompare to handle epoch time.
#ADDED - grabs pictures from CNN.
#ADDED - category doctor health.
#ADDED - remove duplicate titles

import urllib2
import sys,re
import time
import datetime
import MySQLdb

class RssExtractor:
	host="localhost"
	passwd="aggydb"
	user="aggydb"
	db="aggy"
	
	def _RSSFindCat(self,sLink):
		""" returns a category to a link given. 

		"""
		dLinks = {'health':'Health','doctor':'Health', 'sport':'Sports', 'politic':'Politics', 'buisness':'Buisness', \
		'technology':'Technology','science':'Science','entertainment':'Entertainment','art':'Arts'}

		if sLink == "":
			return "General"
		#print sLink

		match = ""
		for key in dLinks.keys():
			match = match + key + "|"
		temp = re.search(match[:-1],sLink)#match[:-1] to remove the last "|"
		if temp and dLinks.has_key(temp.group(0)):
			return dLinks[temp.group(0)]
			
		dCats = {'engadget':'Technology', 'forbes':'Buisness','theverge':'Technology', \
		'polygon':'Games'}
		
		match = ""
		for key in dCats.keys():
			match = match + key + "|"
		temp = re.search(match[:-1],sLink)
		if temp and dCats.has_key(temp.group(0)):
			return dCats[temp.group(0)]

		return "General"

	def RSSRemoveAll(self):
		# connect
		con = MySQLdb.connect(host=self.host, user=self.user, passwd=self.passwd,db=self.db)
		cur = con.cursor()
		cur.execute("SELECT * FROM news_article")
		rows = cur.fetchall()
		if rows:
			for row in rows:
				cur.execute("DELETE FROM news_article WHERE id = %s",(row[0],))
				con.commit()
		if con:
			con.close()

	def RSSUpdateCat(self):
		"""	Update Categories
		that do not have anything in them.

		"""
		# connect
		con = MySQLdb.connect(host=self.host, user=self.user, passwd=self.passwd,db=self.db)
		cur = con.cursor()
		cur.execute("SELECT id,link FROM news_article WHERE category = %s",("",))
		rows = cur.fetchall()
		if rows:
			for row in rows:
				sCat = self._RSSFindCat(row[1])
				cur.execute("UPDATE news_article SET category = %s WHERE id = %s",(sCat,row[0]))
				con.commit()
		if con:
			con.close()

	def RSSPurge(self):
		""" Purge old articles 
		that are so many days old 
		(3 days riht now)

		"""
		con = MySQLdb.connect(host=self.host, user=self.user, passwd=self.passwd,db=self.db)
		cur = con.cursor()
		cur.execute("SELECT id,pubdate FROM news_article")
		rows = cur.fetchall()
		if rows:
			for row in rows:
				if not self._RSSXMLTimeCompare(row[1]):
					#print "will delete: %i" % row[0]
					cur.execute("DELETE FROM news_article WHERE id = %s",(str(row[0]),))
					#cur.execute('DELETE FROM news_article WHERE link = %s',(sLink,))
					con.commit()
		if con:
			con.close()

	def RSSRemoveDupe(self):
		""" Removes duplicate titles
			that have the same name
		"""
		con = MySQLdb.connect(host=self.host, user=self.user, passwd=self.passwd,db=self.db)
		cur = con.cursor()

		#PULL ALL TITLES AND ID FROM TABLE
		cur.execute("SELECT id,title FROM news_article")
		rows = cur.fetchall()
		if rows:
			for row in rows:
				#FIND TITLE WITH SAME NAME
				cur.execute("SELECT id,title FROM news_article WHERE title = %s",(row[1],))
				dupes = cur.fetchall()
				if dupes:
					if len(dupes) < 2:
						continue
					for dupe in dupes:
						#DELETE THAT ARTICLE
						cur.execute("DELETE FROM news_article WHERE id = %s",(dupe[0],))
						con.commit()
						break
		if con:
			con.close()
		
	def _RSSXMLTimeCompare(self,xmlt1,xmlt2 = None):
		""" returns true if xmlt1 is bigger, then repalce the old.
			returns false if xmlt2 is bigger, then don't replace.
			returns false if xmlt1 and xmlt2 are the same.

			if no value is defined for the second argument then
			used to compare against a time of 3 days ago
			returns true if xmlt1 is bigger, then don't delete.
		"""
		#print xmlt1
		#print xmlt2
		if xmlt2 is None:
			xmlt2 = time.strftime("%a, %d %b %Y %H:%M:%S",time.localtime(time.time()- 259200)) 
			#259200 is 3 days

		if isinstance(xmlt1, (int,float)):
			xmlt1 = time.strftime("%a, %d %b %Y %H:%M:%S",time.localtime(xmlt1))

		if isinstance(xmlt2, (int,float)):
			xmlt2 = time.strftime("%a, %d %b %Y %H:%M:%S",time.localtime(xmlt2))

		try:
			dt1= time.strptime(xmlt1[:25],"%a, %d %b %Y %H:%M:%S")
			dt2= time.strptime(xmlt2[:25],"%a, %d %b %Y %H:%M:%S")
			#<pubDate>Thu, 10 Jan 2013 14:55:59 EST</pubDate>
		except ValueError:
			#print "INCORRECT FORMAT"
			return False
		#print dt1
		#print dt2
		#dt = time.struct_time(year,mon,mday,hour,min,sec,wday)
		for num in range(0,5):
			#print num
			#the date is less then current so delete
			if dt1[num] < dt2[num]:
				#print "FALSE"
				return False
			#the date is greater then current 
			elif dt1[num] > dt2[num]:
				#print "TRUE"
				return True
			#the date is equal keep comparing
			else:
				continue
		#print "FALSE"
		return False
		
	def RSSstriptoXML(self,sXml):
		list = []
		if sXml == '':
			return list

		cfi3 = re.compile('''^<meta content="(.*)" itemprop="thumbnailUrl".*/>''',re.MULTILINE)

		for match in re.finditer('<(item|entry)>(.*?)</(item|entry)>',sXml,re.DOTALL):
			s = match.group(0)
			ft = re.search('''<title>(.*?)</title>''',s,re.DOTALL)
			fg = re.search('''<(guid(.*?)|id)>(.*?)</(guid|id)>''',s, re.DOTALL)
			fp = re.search('''<(pubDate|published)>(.*?)</(pubDate|published)>''',s, re.DOTALL)
			#<pubDate>Thu, 10 Jan 2013 14:55:59 EST</pubDate>
			fd = re.search('''<(description|content (.*?))>(.*?)</(description|content)>''',s, re.DOTALL)
			fu = re.search('''<updated>(.*?)</updated>''',s, re.DOTALL)
			fl = re.search('''<link>(.*?)</link>''',s, re.DOTALL)
			fi1 = re.search('''<img(.*?)src=(.*?)"(.*)">''',s,re.DOTALL)
			fi2 = re.search('''<media:thumbnail width="\d{2,}" height="\d{2,}" url="(.*?)"/>''',s,re.DOTALL)	
			list.append('<item>')
			if ft: #title
				#fixing titles that have CDATA in them.
				temp = re.search("<!\[CDATA\[(.*?)\]\]>",ft.group(0),re.DOTALL)
				if temp:
					list[-1] = list[-1] + "<title>"+temp.group(1)+"</title>"
				else:
					list[-1] = list[-1] + ft.group(0)
			if fg: #guid
				list[-1] = list[-1] + fg.group(0)
			if fp: #pub date
				list[-1] = list[-1] + fp.group(0)
			if fu: #updated
				list[-1] = list[-1] + fu.group(0)
			sLink = ''
			if fl: #Link
				list[-1] = list[-1] + fl.group(0)
				sLink = fl.group(1)
			if fd: #description
				list[-1] = list[-1]	+ fd.group(0)

			if fi1: #image
				list[-1] = list[-1]	+ '<image>'+ fi1.group(3) + '</image>'
			elif fi2:
				list[-1] = list[-1]	+ '<image>'+ fi2.group(1) + '</image>'
			else:
				#download the webpage to a textfile
				#print sLink
				sHTML=self.GetPage(sLink,75)
				fi3 = cfi3.search(sHTML)
				if fi3:
					#print "IT WAS A MATCH"
					#print fi3.group(1)
					list[-1] = list[-1]	+ '<image>'+ fi3.group(1) + '</image>'
			list[-1] = list[-1] + '<category>'+self._RSSFindCat(sLink)+'</category>'
			list[-1] = list[-1] + '</item>'
		return list #returns a list of relevent xml info
		
	def RSSSQLAddrecord(self,sXml):
		"""Adds records to database
		
			adds one record to SQL database
			or add many records from a string
		"""
		if sXml == '':
			return ''

		con = MySQLdb.connect(host=self.host, user=self.user, passwd=self.passwd,db=self.db)
		
		for match in re.finditer('<item>(.*?)</item>',sXml, re.DOTALL):
			s = match.group(0)

			sTitle = ''
			sGuid = ''
			sPubdate = ''#time.strftime("%a, %d %b %Y %H:%M:%S",time.localtime(time.time()))
			iPubdate = -1
			sUpdated = ''
			sLink = ''
			sDescription = ''
			sImage = ''
			sCat=''	
			
			fg =re.search('<guid(.*?)>(.*?)</guid>',s, re.DOTALL)
			fp=re.search('<pubDate>(.*?)</pubDate>',s, re.DOTALL)
			fu=re.search('<updated>(.*?)</updated>',s, re.DOTALL)
			ft=re.search('<title>(.*?)</title>',s, re.DOTALL)
			fd=re.search('<description>(.*?)<p>(.*?)</p>(.*?)</description>',s, re.DOTALL)
			fl=re.search('<link>(.*?)</link>',s, re.DOTALL)
			fi=re.search('<image>(.*?)</image>',s,re.DOTALL)
			fc=re.search('<category>(.*?)</category>',s,re.DOTALL)

			if fg: #guid
				sGuid = fg.group(2)
			if fp: #pub date
				#CHANGING PUBDATE TO EPOC TIME
				#print fp.group(1)
				#print time.strptime(fp.group(1)[:25],"%a, %d %b %Y %H:%M:%S")
				iPubdate = time.mktime(time.strptime(fp.group(1)[:25],"%a, %d %b %Y %H:%M:%S"))
				#print iPubdate
				sPubdate = fp.group(1)
			if fu: #updated
				sUpdated =fu.group(1)
			if ft: #title
				sTitle = ft.group(1)
			if fd: #description
				sDescription =fd.group(2)
			if fl: #Link
				sLink = fl.group(1)
			if fi: #image
				sImage = fi.group(1)
			if fc:
				sCat = fc.group(1)

			#do not add to the database if article is too old
			if iPubdate == -1:
				continue
			if not self._RSSXMLTimeCompare(iPubdate):
				continue

			#title,link,description,image,pubdate,rank,category
			sInsert = """INSERT INTO news_article(title,link,
				description,image,pubdate,rank,category)VALUES
				(%s,%s,%s,%s,%s,%s,%s)"""
			cur = con.cursor()

			#if there is an artiticle with the same title, don't put it in the DB
			cur.execute("SELECT pubdate FROM news_article WHERE title = %s",(sTitle,))
			rows = cur.fetchall()
			if rows:
				continue

			cur.execute("SELECT pubdate FROM news_article WHERE link = %s",(sLink,))
			#cur.execute("SELECT id,pubdate FROM news_article")
			rows = cur.fetchall()
			if not rows:
				#print "NEW FEED ENTERED"
				cur.execute(sInsert,(sTitle,sLink,sDescription,sImage,iPubdate,0,sCat))
			else: #somthing was found
				for row in rows:
					#check if the dates
					if self._RSSXMLTimeCompare(iPubdate,row[0]):
						cur.execute('DELETE FROM news_article WHERE link = %s',(sLink,))
						cur.execute(sInsert,(sTitle,sLink,sDescription,sImage,iPubdate,0,sCat))
						#print "delete the old, in with the new"
						#delete the

			con.commit()
		if con:
			con.close()

	def GetPage(self,sUrl,iLines = None):
		if sUrl == "":
			return ""
		try:
			#assigns the url to a request object
			req = urllib2.Request(sUrl) 
			#retreve the webpage.
			response = urllib2.urlopen(req,None,5) 
		except IOError:
			return ""
		if iLines == None:
			return response.read()
		sResponse = ""
		for i in xrange(iLines):
			sResponse += response.readline()
		return sResponse

	def RSSrun(self,file):
		lFeeds = [line.strip() for line in open(file,'r')]
		i=0
		for sLink in lFeeds:
			i +=1
			#print sLink
			info = self.RSSstriptoXML(self.GetPage(sLink))
			if info == '':
				print 'Error Reading Link'
			for section in info:
				#print "ADDING TO SECTION"
				self.RSSSQLAddrecord(section)

def main():
	db = RssExtractor()
	db.RSSRemoveDupe()
	db.RSSrun("feeds.txt")
	db.RSSPurge()
	
if __name__== "__main__":
	main()


