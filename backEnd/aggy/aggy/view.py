from django.contrib.auth.models import User
from news.models import CustomArticle
from news.models import Article
from news.models import Friends
from news.models import Liked

class CustomView:

  def addArticle(self, article, time, user):
    try:
      theArticle = CustomArticle.objects.get(user=user, article=article);
      theArticle.rank = theArticle.rank + article.rank
      theArticle.save()
    except CustomArticle.DoesNotExist:
      theArticle = CustomArticle(user=user, article=article, rank=time)
      theArticle.save()

  def runCode(self, username):
    user = User.objects.get(username__exact=username)
    CustomArticle.objects.filter(user=user).delete()
    articles = Article.objects.all()
    friends = Friends.objects.filter(user=user)

    for i in friends:
      liked = Liked.objects.filter(user=i.friend)
      for j in liked:
        self.addArticle(j.article, j.time, user)

    for i in articles:
      self.addArticle(i, i.rank, user)

  def __init__(self, username):
    self.runCode(username)

