from tastypie.authorization import Authorization
from tastypie.exceptions import Unauthorized

class UserStuff(Authorization):
  def read_list(self, object_list, bundle):
    return object_list.filter(user=bundle.request.user)


