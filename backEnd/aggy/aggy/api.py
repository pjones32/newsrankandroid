# aggy/api.py
from django.conf.urls.defaults import patterns, url
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from tastypie.authentication import ApiKeyAuthentication
from tastypie.authentication import BasicAuthentication
from tastypie.resources import ModelResource, ALL
from tastypie.utils import trailing_slash
from tastypie.throttle import CacheDBThrottle
from tastypie.models import ApiKey
from tastypie import fields
from news.models import Article
from news.models import Friends
from news.models import Liked
from news.models import CustomArticle
from aggy.viewfaster import CustomView
from aggy.userstuff import UserStuff
import re

EMAIL_REGEX = re.compile("[^@]+@[^@]+\.[^@]+")

class ArticleResource(ModelResource):
  class Meta:
    queryset = Article.objects.all()
    resource_name = 'article'

class ATimeResource(ModelResource):
  class Meta:
    resource_name = 'time'
    authentication = ApiKeyAuthentication()

  def override_urls(self):
    return [
        url(r"^(?P<resource_name>%s)/add%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('addtime'), name="api_addtime"),
        ]

  def addtime(self, request, **kwargs):
    if not (('username' in request.GET) and ('api_key' in request.GET) and ('time' in request.GET) and ('article' in request.GET)):
      return self.create_response(request, {"Malformed_Request": "True"})

    try:
      article = Article.objects.get(pk=int(request.GET['article']))
    except Article.DoesNotExists:
      return self.create_response(request, {"Badarticle": request.GET['article']})

    try:
      user = User.objects.get(username__exact=request.GET['username'])
    except User.DoesNotExists:
      return self.create_response(request, {"Badusername": request.GET['username']})

    if (int(request.GET['time']) >= 5000):
      time = 1

    try:
      exist = Liked.objects.get(user=user, article=article)
      exist.time = exist.time + time
      exist.save()
    except Liked.DoesNotExist:
      newEntry = Liked(user=user, article=article, time=time)
      newEntry.save()

    article.rank = article.rank + 1
    article.save()
    return self.create_response(request, {"Confirmed": "1"})

class FriendResource(ModelResource):
  class Meta:
    resource_name = 'friend'
    authentication = ApiKeyAuthentication()

  def override_urls(self):
    return [
        url(r"^(?P<resource_name>%s)/add%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('addfriend'), name="api_addfriend"),
      ]

  def addfriend(self, request, **kwargs):
    if not (('username' in request.GET) and ('friend' in request.GET)):
      return self.create_response(request, {"Malformed_Request": "True"})

    userName = request.GET['username']
    friendName = request.GET['friend']

    user = User.objects.get(username__exact=userName)
    friend = User.objects.get(username__exact=friendName)

    if (friend is None) or (user is None): 
      return self.create_response(request, {"Friend does not exist!": "1"})
    try:
      Friends.objects.get(user=user, friend=friend)
    except Friends.DoesNotExist:
      newEntry = Friends(user=user, friend=friend, status=True)
      newEntry.save()
      return self.create_response(request, {"complete": "1"})

    return self.create_response(request, {"Already Friends" : "1"})

class UserResource(ModelResource):
  class Meta:
    resource_name = 'user'
    filtering = {
    }


  def override_urls(self):
    return [
      url(r"^(?P<resource_name>%s)/signup%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('signup'), name="api_signup"),
      url(r"^(?P<resource_name>%s)/reqkey%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('reqkey'), name="api_reqkey"),
    ]
  def signup(self, request, **kwargs):
    throttle = CacheDBThrottle(throttle_at=3, timeframe=7200)
    created = "0"
    inUse = "0"
    throttled = "0"

    if not (('email' in request.GET) and ('password' in request.GET) and ('first' in request.GET) and ('last' in request.GET)):
      return self.create_response(request, {"Malformed_Request": "True"})

    email = request.GET['email']
    password = request.GET['password']
    firstname = request.GET['first']
    lastname = request.GET['last']

  # if not throttle.should_be_throttled(host):
    doesExist = User.objects.filter(username=email)
    
    if len(doesExist) == 1:
      inUse = "1"
    elif EMAIL_REGEX.match(email):
    #throttle.accessed(host)
      tempUser = User.objects.create_user(email,email,password)
      tempUser.last_name = lastname;
      tempUser.first_name = firstname;
      tempUser.save();
      created = "1"
  #else:
    throttled = "1"

    return self.create_response(request, {"throttled": throttled, "in_use": inUse, "created": created})

  def reqkey(self, request, **kwargs):
    if (not 'email' in request.GET) or (not 'password' in request.GET):
      return self.create_response(request, {"Malformed_Request": "True"})
    
    email = request.GET['email']
    password = request.GET['password']
    
    user = authenticate(username=email, password=password)

    if user is not None:
      try:
        key = ApiKey.objects.get(user=user)
      except ApiKey.DoesNotExist:
        key = ApiKey.objects.create(user=user)
        key.save()

      return self.create_response(request, {"APIKey" : key.key, "Name" : user.get_full_name()})

    else:
      return self.create_response(request, {"failed" : "1"})

class FriendListUserResource(ModelResource):
  class Meta:
    queryset = User.objects.all()
    resource_name = 'user'
    excludes = ['email', 'password', 'is_active', 'is_staff', 'is_superuser', 'last_login', 'resource_uri']


class FriendListResource(ModelResource):
  friend = fields.ForeignKey(FriendListUserResource, 'friend', full=True)
  class Meta:
    queryset = Friends.objects.all()
    resource_name = 'friendlist'
    authentication = ApiKeyAuthentication()
    authorization = UserStuff()

class CustomArticleModelResource(ModelResource):
  class Meta:
    queryset = Article.objects.all()
    resource_name = 'customarticlemodel'

class CustomArticleResource(ModelResource):
  article = fields.ForeignKey(CustomArticleModelResource, 'article', full=True)
  class Meta:
    queryset = CustomArticle.objects.all()
    resource_name = 'articlecustom'
    authentication = ApiKeyAuthentication()
    authorization = UserStuff()

  def get_object_list(self, request):
    CustomView(request.GET['username'])
    return super(CustomArticleResource, self).get_object_list(request).filter(user=User.objects.get(username__exact=request.GET['username']))

